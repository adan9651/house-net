import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, PatternValidator } from '@angular/forms';
import { Service } from '../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-house-form',
  templateUrl: './house-form.component.html',
  styleUrls: ['./house-form.component.css'],
  styles: [`.ng-invalid.ng-touched:not(form){ border: 1px solid red; }`]
})
export class HouseFormComponent implements OnInit {

  forma:FormGroup;

  constructor(private fb: FormBuilder, private service: Service, private router: Router) { 
    this.createForm();
  }

  ngOnInit() {
  }

  add(){
    this.service.addHouse(this.forma.value);
  }

  estados = [
    {nombre:"Aguascalientes"},
    {nombre:"Baja California"},
    {nombre:"Baja California Sur"},
    {nombre:"Campeche"},
    {nombre:"Coahuila de Zaragoza"},
    {nombre:"Colima"},
    {nombre:"Chiapas"},
    {nombre:"Chihuahua"},
    {nombre:"Distrito Federal"},
    {nombre:"Durango"},
    {nombre:"Guanajuato"},
    {nombre:"Guerrero"},
    {nombre:"Hidalgo"},
    {nombre:"Jalisco"},
    {nombre:"México"},
    {nombre:"Michoacán de Ocampo"},
    {nombre:"Morelos"},
    {nombre:"Nayarit"},
    {nombre:"Nuevo León"},
    {nombre:"Oaxaca"},
    {nombre:"Puebla"},
    {nombre:"Querétaro"},
    {nombre:"Quintana Roo"},
    {nombre:"San Luis Potosí"},
    {nombre:"Sinaloa"},
    {nombre:"Sonora"},
    {nombre:"Tabasco"},
    {nombre:"Tamaulipas"},
    {nombre:"Tlaxcala"},
    {nombre:"Veracruz de Ignacio de la Llave"},
    {nombre:"Yucatán"},
    {nombre:"Zacatecas"}
  ]

  rooms = [
    {num: 1},
    {num: 2},
    {num: 3},
    {num: 4},
    {num: 5},
    {num: 6},
    {num: 7},
    {num: 8}
  ]

  bathrooms = [
    {num: 1},
    {num: 1.5},
    {num: 2},
    {num: 2.5},
    {num: 3},
    {num: 3.5},
    {num: 4},
    {num: 4.5}
  ]

  floors = [
    {num: 1},
    {num: 2},
    {num: 3},
    {num: 4},
  ]

  tf = [
    {bool: "Si"},
    {bool: "No"}
  ]

  typeHouse = [
    {option: "Casa"},
    {option: "Departamento"},
    {option: "Casa en condominio"}
  ]

  createForm(){
    this.forma = this.fb.group({
      'calle': new FormControl('', [Validators.required, Validators.pattern("^[a-z áéíóúÁÉÍÓÚA-Z0-9.]{2,40}$")]),
      'num': new FormControl('', [Validators.required, Validators.pattern("^[0-9]{1,5}$")]),
      'colonia': new FormControl('', [Validators.required, Validators.pattern("^[a-z áéíóúÁÉÍÓÚA-Z0-9.]{2,40}$")]),
      'cp': new FormControl('', [Validators.nullValidator, Validators.pattern("^[0-9]{5}$")]),
      'estado': new FormControl('', Validators.required),
      'numHabitaciones': new FormControl('', Validators.required),
      'bano': new FormControl('', Validators.required),
      'pisos': new FormControl('', Validators.required),
      'piscina': new FormControl('', Validators.required),
      'garage': new FormControl('', Validators.required),
      'tipo': new FormControl('', Validators.required),
      'jardin': new FormControl('', Validators.required),
      'mensualidad': new FormControl('', [Validators.nullValidator, Validators.pattern("^[0-9]{1,7}$")]),
      'tiempo': new FormControl('', [Validators.nullValidator, Validators.pattern("^[0-9]{1,2}$")]),
      'url': new FormControl(''),
      'icon': new FormControl(''),
      'owner': new FormControl('')
    })
  }

}
