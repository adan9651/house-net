import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-alta',
  templateUrl: './alta.component.html',
  styleUrls: ['./alta.component.css']
})
export class AltaComponent implements OnInit {

  constructor(private fb: FormBuilder) {
    this.createForm();
   }

  user;
  forma:FormGroup;

  ngOnInit() {
    this.user = localStorage.getItem('currentUser');
  }

  vehiculo:Object = {
    tipo: "Auto y Suv",
    marca: "Nissan",
    year: "2020",
    dec: "Si",
    conductor: "Conductora",
    modelo: "Tsuru",
    seguro: "Amplia Plus"
  }
  seguros = [{cod:"4",nombre: "Amplia Plus"},{cod:"3",nombre: "Amplia"},{cod:"2",nombre: "Limitada"},{cod:"1",nombre: "Responsabilidad Civil"}];
  conductores = [{cod:"1",nombre:"Conductor"},{cod:"2",nombre:"Condctora"}]
  decs = [{cod:"1",nombre:"Si"},{cod:"2",nombre:"No"}];
  tipos=[{cod: "1",nombre: "Auto y SUV"},{cod: "2",nombre: "Pickup"}];
  years = [{nombre:"2011"},{nombre:"2013"},{nombre:"2014"},{nombre:"2015"},{nombre:"2016"},{nombre:"2017"},{nombre:"2018"},{nombre:"2019"},{nombre:"2020"}]
  marcas = [
    {cod: "1",nombre: "Acura"},
    {cod: "2",nombre: "Alfa Romeo"},
    {cod: "3",nombre: "Audi"},
    {cod: "4",nombre: "BAIC"},
    {cod: "5",nombre: "Bentley"},
    {cod: "6",nombre: "BMW"},
    {cod: "7",nombre: "Buick"},
    {cod: "8",nombre: "Cadillac"},
    {cod: "9",nombre: "Chevrolet"},
    {cod: "10",nombre: "Chrysler"},
    {cod: "11",nombre: "Dodge"},
    {cod: "12",nombre: "Ferrari"},
    {cod: "13",nombre: "Fiat"},
    {cod: "14",nombre: "Ford"},
    {cod: "15",nombre: "GEO"},
    {cod: "16",nombre: "GMC"},
    {cod: "17",nombre: "Honda"},
    {cod: "18",nombre: "Hummer"},
    {cod: "19",nombre: "Hyundai"},
    {cod: "20",nombre: "Infiniti"},
    {cod: "21",nombre: "Jaguar"},
    {cod: "22",nombre: "Jeep"},
    {cod: "23",nombre: "Nissan"},
    {cod: "24",nombre: "McLaren"},
    {cod: "25",nombre: "Subaru"},
  ]

  createForm(){
    this.forma = this.fb.group({
      'tipo': new FormControl('',Validators.required),
      'marca': new FormControl('',Validators.required),
      'year': new FormControl('',Validators.required),
      'dec': new FormControl('',Validators.required),
      'conductor': new FormControl('',Validators.required),
      'modelo': new FormControl('',Validators.required),
      'seguro': new FormControl('',Validators.required),
    } )
  }

  cotizar(){
    console.log(this.forma.value.tipo)
    console.log(this.forma.value.marca);
    console.log(this.forma.value.dec);
    console.log(this.forma.value.conductor);
    console.log(this.forma.value.seguro);
    var comision = this.forma.value.marca/100 + this.forma.value.tipo/100 + this.forma.value.dec/100 + this.forma.value.conductor/2 + this.forma.value.seguro*5;
    var total = 5000 * comision;
    console.log(total);
    alert("El valor de la cotizacion anual del seguro es de "+ total + " pesos");
  }

}
