import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Service } from '../app.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Customer } from '../model/customer';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  styles: [`.ng-invalid.ng-touched:not(form) { border: 1px solid red; }`]
})
export class LoginComponent implements OnInit {

  forma: FormGroup
  public array;
  customer: Customer;

  constructor( private fb: FormBuilder, private service: Service, private router: Router, private http: HttpClient ) { 
    this.forma = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._-]+@([a-z0-9.-]+\.)[a-z]{3}$")]),
      'password': new FormControl('',Validators.required)
    } )
  }

  async findUser(){
    var email = this.forma.value.email;
    var password = this.forma.value.password;
    console.log("datos del usuario ",this.forma.value);
    const res = await this.service.findUser(email);
    this.array = res;
    if(this.array != undefined){
      if(this.array['password'] != password){
        alert("password incorrecto!")
      }else{
        this.router.navigate(['/home']);
        localStorage.setItem('currentUser',email);
      }
    }else{ 
        alert("email incorrecto!");
    }
  }

  ngOnInit() {

  }

}
