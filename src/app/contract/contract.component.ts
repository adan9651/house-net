import { Component, OnInit } from '@angular/core';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.css']
})
export class ContractComponent {

  generatePdf(){
    const documentDefinition = { content: 'CONTRATO DE ARRENDAMIENTO\n\nCONTRATO DE ARRENDAMIENTO ' + 
    'QUE CELEBRAN POR UNA PARTE, NOMBRE ARRENDADOR, A QUIEN EN LO SUCESIVO SE LES DENOMINARA EL “ARRENDADOR”' +
    ' Y DE LA OTRA PARTE, LA EMPRESA DENOMINADA “HOUSE-NET”, COMO INTERMEDIARIO, TAMBIÉN LA PERSONA QUE GOZARÁ' +
    ' LOS DERECHOS POR EL TIEMPO SOLICITADO, NOMBRE ARRENDATARIO, A QUIEN EN LO SUCESIVO SE LE DENOMINARA EL “' + 
    'ARRENDATARIO”; AL TENOR DE LAS SIGUIENTES DECLARACIONES:\n\nDECLARACIONES\n\n1.- El “ARRENDADOR” DECLARA que:\n\n' + 
    'A) Es persona física, con capacidad jurídica para arrendar bienes inmuebles.\n\nB) Es legítima propietaria del' +
    ' INMUEBLE ubicado en DIRECCIÓN, con las ESPECIFICACIONES MOSTRADAS EN LA WEB.\n\nC) El objeto del presente' +
    ' contrato de arrendamiento lo es el bien inmueble antes descrito.\n\nD) Tiene la libre disponibilidad sobre el ' +
    'local comercial materia de este Contrato, sin limitaciones ni responsabilidades de naturaleza civil, mercantil' +
    ', física o laboral de ninguna índole.\n\nE) Es deseo dar en arrendamiento el mismo local comercial, con sujeción ' +
    'a los términos y condiciones contenidas en este documento.\n\nF) El contrato en turno comienza a contar desde el ' +
    'mismo día en que se estipula y firma este contrato.\n\n2.- El ARRENDATARIO, DECLARA que:\n\nA) Conoce perfectamente' +
    ' el local comercial objeto del presente Contrato.\n\nB) Desea celebrar este arrendamiento con el fin de que se le' +
    ' otorgue el uso del local comercial arrendado por un período de TIEMPO, a partir de la firma del presente contrato' +
    ', así mismo, declarar que se tiene que pagar 2 meses de anticipo de lo de la renta del inmueble, el cual tiene el ' +
    'precio por mes de $PRECIO.\n\nC) Su representada es una persona jurídica debidamente constituida conforme a la ' +
    'legislación aplicable.\n\n\n\n\n______________________________\n"El Arrendatario"\nNOMBRE DE ARRENDATARIO'};
    pdfMake.createPdf(documentDefinition).open();
  }

}
