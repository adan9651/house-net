import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Service } from '../app.service';
import { getLocaleMonthNames } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  forma:FormGroup;
  currentUser;
  logged = false;
  public array;
  public page: Number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente
  public page2: Number = 2; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente
  public totalPages: Number; //Número total de páginas
  public numShops: number; //Total de tiendas existentes
  private numResults: number = 3;
  items = [];
  pageOfItems: Array<any>;

  goToPage(page: number){
    this.page = page;
    this.getShopsByPage(page);
  }

  async getShopsByPage(page: Number) {
    this.array = await this.service.buscaCasas(this.forma.value);
    var tam = this.array
    this.numShops = tam.length;
    this.totalPages = Math.round(this.numShops / this.numResults);
  }

  constructor(private fb: FormBuilder, private service: Service, private router: Router) { 
    this.createForm();
  }

  createForm(){
    this.forma = this.fb.group({
      'estado': new FormControl("Zacatecas",Validators.required),
      'numHabitaciones': new FormControl('1',Validators.required),
      'bano': new FormControl('1',Validators.required),
      'tiempo': new FormControl('1',Validators.required),
      'tipo': new FormControl('casa',Validators.required),
      'mensualidad': new FormControl('1',Validators.required),
      'maxMens': new FormControl(),
      'minMens': new FormControl(),
      'maxHabs': new FormControl(),
      'minHabs': new FormControl(),
      'maxTiempo': new FormControl(),
      'minTiempo': new FormControl()
    })
  }

  async buscar(){
    this.calculaMonto();
    this.calculaHabitaciones();
    this.calculaTiempo();
    this.array = await this.service.buscaCasas(this.forma.value);
    this.items = Array(this.array.length).fill(0).map((x, i) => ({ id: (i + 1), name: `Item ${i + 1}`}));
    this.totalPages = Math.round(this.numShops / this.numResults);
  }
  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }
  
  calculaHabitaciones(){
    var x = +this.forma.value.bano;
    this.forma.value.bano = x;
    switch(this.forma.value.numHabitaciones){
      case '1':
        this.forma.value.minHabs = 1;
        this.forma.value.maxHabs = 2;
        break;
      
      case '2':
        this.forma.value.minHabs = 3;
        this.forma.value.maxHabs = 4;
        break;

      case '3':
        this.forma.value.minHabs = 5;
        this.forma.value.maxHabs = 100;
        break;
    }
  }

  calculaTiempo(){
    switch(this.forma.value.tiempo){
      case '1':
        this.forma.value.minTiempo = 0;
        this.forma.value.maxTiempo = 6;
        break;

      case '2':
        this.forma.value.minTiempo = 6;
        this.forma.value.maxTiempo = 12;
        break;

      case '3':
        this.forma.value.minTiempo = 12;
        this.forma.value.maxTiempo = 24;
        break;

      case '4':
        this.forma.value.minTiempo = 24;
        this.forma.value.maxTiempo = 120;
        break;
    } 
  }

  calculaMonto(){
    switch(this.forma.value.mensualidad){
      case '1':
        this.forma.value.minMens = 0;
        this.forma.value.maxMens = 5;
        break;
      
      case '2':
        this.forma.value.minMens = 5;
        this.forma.value.maxMens = 10;
        break;

      case '3':
        this.forma.value.minMens = 10;
        this.forma.value.maxMens = 20;
        break;

      case '4':
        this.forma.value.minMens = 20;
        this.forma.value.maxMens = 9000;
        break;
    }
  }

  async ngOnInit() {
    this.currentUser = localStorage.getItem('currentUser');
    if(this.currentUser != undefined){
      this.logged = true;
    }
    this.array = await this.service.getAllRealEstates("Zacatecas");
  }

  login(){
    this.router.navigate(['/login']);
  }

  registrar(){
    this.router.navigate(['/registro']);
  }

  logout(){
    this.router.navigate(['/home']);
    localStorage.clear();
    window.location.reload();
  }

  perfil(){
    this.router.navigate(['/perfil']);
  }

  tipos = [{nombre:"casa"},{nombre:"casa en condominio"},{nombre:"departamento"}]

  estados = [
    {cod: "1",nombre:"Aguascalientes"},
    {cod: "2",nombre:"Baja California"},
    {cod: "3",nombre:"Baja California Sur"},
    {cod: "4",nombre:"Campeche"},
    {cod: "5",nombre:"Coahuila de Zaragoza"},
    {cod: "6",nombre:"Colima"},
    {cod: "7",nombre:"Chiapas"},
    {cod: "8",nombre:"Chihuahua"},
    {cod: "9",nombre:"Distrito Federal"},
    {cod: "10",nombre:"Durango"},
    {cod: "11",nombre:"Guanajuato"},
    {cod: "12",nombre:"Guerrero"},
    {cod: "13",nombre:"Hidalgo"},
    {cod: "14",nombre:"Jalisco"},
    {cod: "15",nombre:"México"},
    {cod: "16",nombre:"Michoacán de Ocampo"},
    {cod: "17",nombre:"Morelos"},
    {cod: "18",nombre:"Nayarit"},
    {cod: "19",nombre:"Nuevo León"},
    {cod: "20",nombre:"Oaxaca"},
    {cod: "21",nombre:"Puebla"},
    {cod: "22",nombre:"Querétaro"},
    {cod: "23",nombre:"Quintana Roo"},
    {cod: "24",nombre:"San Luis Potosí"},
    {cod: "25",nombre:"Sinaloa"},
    {cod: "26",nombre:"Sonora"},
    {cod: "27",nombre:"Tabasco"},
    {cod: "28",nombre:"Tamaulipas"},
    {cod: "29",nombre:"Tlaxcala"},
    {cod: "30",nombre:"Veracruz de Ignacio de la Llave"},
    {cod: "31",nombre:"Yucatán"},
    {cod: "32",nombre:"Zacatecas"}
  ]

}
