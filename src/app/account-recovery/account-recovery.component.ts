import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Service } from '../app.service';
import { Customer } from '../model/customer';

@Component({
  selector: 'app-account-recovery',
  templateUrl: './account-recovery.component.html',
  styleUrls: ['./account-recovery.component.css']
})
export class AccountRecoveryComponent implements OnInit {

  Customer: Customer;
  forma: FormGroup;
  public array;

  constructor(private fb: FormBuilder, private service: Service, private router: Router) { 
    this.createForm();
  }

  ngOnInit() {
  }

  createForm(){
    this.forma = this.fb.group({
      'email': new FormControl('', [Validators.required,  Validators.pattern("^[a-z0-9._-]+@([a-z0-9.-]+\.)[a-z]{3}$")])
    })
  }

  async findUser(){
    var email = this.forma.value.email;
    const res = await this.service.findUser(email);
    this.array = res;
    if(this.array != undefined){
      this.service.recuperaCuenta(this.array['email'], this.array['password'], this.array['nombres'],this.array['apePat'],this.array['apeMat']);
      //this.service.recuperaCuenta(email);
      alert("Hemos enviado un correo a " + email);
      this.router.navigate(['/login']);
    }
    else{
      alert("El correo no existe");
    }
  }

}
