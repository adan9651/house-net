import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, PatternValidator } from '@angular/forms';
import { Service } from '../app.service';
import { Customer } from '../model/customer';
import { Router } from '@angular/router';
import * as $ from 'jquery' ;

@Component({
  selector: 'app-customer-reg',
  templateUrl: './customer-reg.component.html',
  styleUrls: ['./customer-reg.component.css'],
  styles: [`.ng-invalid.ng-touched:not(form){ border: 1px solid red; }`]
})
export class CustomerRegComponent implements OnInit {

  customer: Customer;
  forma:FormGroup;
  public array1;
  public array2;
  public array3;
  public array4;
  succes

  constructor(private fb: FormBuilder, private service: Service, private router: Router) { 
    this.createForm();

    this.forma.controls['email2'].setValidators([
      Validators.required,
      this.correoDiferente.bind(this.forma)
    ])

    this.forma.controls['password2'].setValidators([
      Validators.required,
      this.passDiferente.bind(this.forma),
    ])

  }

  ngOnInit() {
  }
    
  registrar(){
    this.service.addCustomer(this.forma.value);
    this.router.navigate(['/login']);
  }

  /*estados = [
    {nombre:"Aguascalientes"},
    {nombre:"Baja California"},
    {nombre:"Baja California Sur"},
    {nombre:"Campeche"},
    {nombre:"Coahuila de Zaragoza"},
    {nombre:"Colima"},
    {nombre:"Chiapas"},
    {nombre:"Chihuahua"},
    {nombre:"Distrito Federal"},
    {nombre:"Durango"},
    {nombre:"Guanajuato"},
    {nombre:"Guerrero"},
    {nombre:"Hidalgo"},
    {nombre:"Jalisco"},
    {nombre:"México"},
    {nombre:"Michoacán de Ocampo"},
    {nombre:"Morelos"},
    {nombre:"Nayarit"},
    {nombre:"Nuevo León"},
    {nombre:"Oaxaca"},
    {nombre:"Puebla"},
    {nombre:"Querétaro"},
    {nombre:"Quintana Roo"},
    {nombre:"San Luis Potosí"},
    {nombre:"Sinaloa"},
    {nombre:"Sonora"},
    {nombre:"Tabasco"},
    {nombre:"Tamaulipas"},
    {nombre:"Tlaxcala"},
    {nombre:"Veracruz de Ignacio de la Llave"},
    {nombre:"Yucatán"},
    {nombre:"Zacatecas"}
  ]*/

  vencimientoMes = [
    {mes: "01"},
    {mes: "02"},
    {mes: "03"},
    {mes: "04"},
    {mes: "05"},
    {mes: "06"},
    {mes: "07"},
    {mes: "08"},
    {mes: "09"},
    {mes: "10"},
    {mes: "11"},
    {mes: "12"}
  ]

  vencimientoAnio = [
    {anio: "20"},
    {anio: "21"},
    {anio: "22"},
    {anio: "23"},
    {anio: "24"}
  ]

  createForm(){
    this.forma = this.fb.group({
      'checkbox': new FormControl('', Validators.requiredTrue),
      'user': new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9_-]{3,16}$")]),
      'password': new FormControl('', Validators.required),
      'password2': new FormControl('', Validators.required),
      'nombres': new FormControl('', [Validators.required, Validators.pattern("^[a-z áéíóúÁÉÍÓÚA-Z.]{2,20}$")]),
      'apePat': new FormControl('', [Validators.required, Validators.pattern("^[a-z áéíóúÁÉÍÓÚA-Z.]{2,20}$")]),
      'apeMat': new FormControl('', [Validators.nullValidator, Validators.pattern("^[a-z áéíóúÁÉÍÓÚA-Z.]{2,20}$")]),
      /*'calle': new FormControl('', [Validators.required, Validators.pattern("^[a-z áéíóúÁÉÍÓÚA-Z0-9.]{2,40}$")]),
      'numExt': new FormControl('', [Validators.required, Validators.pattern("^[0-9]{1,5}$")]),
      'numInt': new FormControl('', [Validators.nullValidator, Validators.pattern("^[a-zA-Z0-9-]{1,3}$")]),
      'fraccionamiento': new FormControl('', [Validators.required, Validators.pattern("^[a-z áéíóúÁÉÍÓÚA-Z0-9.]{2,40}$")]),
      'municipio': new FormControl('', [Validators.required, Validators.pattern("^[a-z áéíóúÁÉÍÓÚA-Z.]{2,40}$")]),
      'estado': new FormControl('', Validators.required),
      'codPostal': new FormControl('', [Validators.nullValidator, Validators.pattern("^[0-9]{5}$")]),*/
      'email': new FormControl('', [Validators.required,  Validators.pattern("^[a-z0-9._-]+@([a-z0-9.-]+\.)[a-z]{3}$")]),
      'email2': new FormControl('', Validators.required),
      'telefono': new FormControl('', [Validators.required, Validators.pattern("^[0-9]{10}$")]),
      'numTarjeta': new FormControl('', [Validators.required, Validators.pattern("^[0-9]{16}$")]),
      'vencimientoAA': new FormControl('', Validators.required),
      'vencimientoMM': new FormControl('', Validators.required),
      'cvv': new FormControl('', [Validators.required, Validators.pattern("^[0-9]{3}$")]),
      'tipo': new FormControl('1',Validators.required)
    } )
  }

  async findUser(){
    this.succes = false;
    var email = this.forma.value.email;
    var user = this.forma.value.user;
    var numTarjeta = this.forma.value.numTarjeta;
    const res1 = await this.service.findUser(email);
    const res2 = await this.service.findUsuario(user);
    const res3 = await this.service.findNumTarjeta(numTarjeta);
    this.array1 = res1;/*Correo*/
    this.array2 = res2;/*Usuario*/
    this.array3 = res3;/*Tarjeta*/
    if((this.array1 != undefined) && (this.array2 != undefined) && (this.array3 != null)){/*Si encontró todo*/
      alert("Usuario, Correo y Número de Tarjeta ya existen");
    }
    else if((this.array1 != undefined) && (this.array2 != undefined)){/*Si encuentra correo y usuario*/
      alert("Usuario y Correo ya existen");
    }
    else if((this.array1 != undefined) && (this.array3 != undefined)){/*Si encuentra correo y tarjeta*/
      alert("Correo y Tarjeta ya existen");
    }
    else if((this.array2 != undefined) && (this.array3 != undefined)){/*Si encuentra usuario y tarjeta*/
      alert("Usuario y Tarjeta ya existen");
    }
    else if(this.array1 != undefined){
      alert("Correo ya existe");
    }
    else if(this.array2 != undefined){
      alert("Usuario ya existe");
    }
    else if(this.array3 != undefined){
      alert("Tarjeta ya existe");
    }
    else{
      this.array4 = await this.service.validateCard(this.forma.value.numTarjeta);
      const resp = this.array4;
      if(resp == 200){
        alert("Register success!");
        this.succes = true;
        this.registrar();
      }else{
        alert("Tarjeta invalida");
      }
    }
  }

  correoDiferente( control: FormControl ): { [s:string]:boolean }  {
    let forma:any = this;
    if( control.value !== forma.controls['email'].value ){
      return {
        correoDiferente: true
      }
    }
    return null;
  }

  passDiferente(control: FormControl): { [s:string]:boolean}{
    let forma:any = this;
    if(control.value !== forma.controls['password'].value){
      return {
        passDiferente: true
      }
    }
    return null;
  }
}
