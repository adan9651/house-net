const mongoose = require('mongoose');

const URI = 'mongodb://localhost/base';

mongoose.connect(URI)
.then(db => console.log("conectado a base de datos de mongo"))
.catch(err => console.error(err));

module.exports = mongoose;