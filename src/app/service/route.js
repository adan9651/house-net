
const express = require('express');
const app = express();
const userRoutes = express.Router();

// Require Business model in our routes module
let User = require('../model/model.user');
let Rolas = require('../model/model.rolas');
let Artistas = require('../model/model.artista');
let Playlist = require('../model/model.mylist');
let Pagos = require('../model/model.pagos');
console.log("entra");
// Defined store route
userRoutes.route('/add/user').post(function (req, res) {
  let user = new User(req.body);
  console.log(req.body);
  user.save()
    .then(business => {
      res.status(200).json({'business': 'business in added successfully'});
    })
    .catch(err => {
    res.status(400).send("unable to save to database");
    });
});


userRoutes.route('/add/editUser').post(function (req, res) {
  let id_user = req.body.id_user;
  let nombre_usuario = req.body.nombre_usuario;
  let email = req.body.email;
  let pass = req.body.pass;
  console.log(id_user,nombre_usuario,email,pass);
  User.update({"id_user":id_user},{$set:{"nombre_usuario":nombre_usuario,"email":email,"pass":pass}},function (err, users){
    if(err){
      console.log(err);
    }
    else {
      res.json(users);
    }
  });
  });
// Defined get data(index or listing) route
userRoutes.route('/').get(function (req, res) {
  User.find(function (err, users){
    if(err){
      console.log(err);
    }
    else {
      res.json(users);
    }
  });
});

// Defined get data(index or listing) route
userRoutes.route('/editUser/').get(function (req, res) {
  let id = req.params.username;
    User.find({},function (err, users){
    if(err){
      console.log(err);
    }
    else {
      res.json(users);
    }
  });
});

// Defined get data(index or listing) route
userRoutes.route('/username/:username').get(function (req, res) {
  let id = req.params.username;
    User.find({"nombre_usuario":id},function (err, users){
    if(err){
      console.log(err);
    }
    else {
      res.json(users);
    }
  });
});

// Defined edit route
userRoutes.route('/edit/:id').get(function (req, res) {
  let id = req.params.id;
  User.findById(id, function (err, users){
      res.json(users);
  });
});

userRoutes.route('/suscripcion/:id').get(function (req, res) {
  let id = req.params.id;
  console.log("entra a susc en js");
  Pagos.find({"id_user":id}, function (err, users){
      res.json(users);
  });
});

//  Defined update route
userRoutes.route('/update/:id').post(function (req, res) {
    User.findById(req.params.id, function(err, business) {
    if (!business){
      console.log('Could not load Document')
    }else {
        business.person_name = req.body.person_name;
        business.business_name = req.body.business_name;
        business.business_gst_number = req.body.business_gst_number;
        business.save().then(business => { res.json('Update complete'); })
      .catch(err => { res.status(400).send("unable to update the database");});
    }
  });
});

// Defined delete | remove | destroy route
userRoutes.route('/eliminaUser/:id').get(function (req, res) {
    User.remove({email: req.params.id}, function(err, user){
        if(err) res.json(err);
        else console.log("usuario eliminado");

    });
});


userRoutes.route('/findUser/:email').get(function (req, res) {
  console.log("entra a ruta finduser js");
  let email = req.params.email;
  console.log("correo de user "+email);
  User.find({"email":email}, function (err, users){
      console.log("Entra a get user de user.route");
      res.json(users);
      console.log("resultado ",users);
  });
});


userRoutes.route('/findMaxUser').get(function (req, res) {
  User.find( function (err, users){
      console.log("Entra a Max user");
      res.json(users);
      console.log("resultado ",users);
  }).sort({id_user:-1}).limit(1);
});

userRoutes.route('/findRola/:rola').get(function (req, res) {
  console.log("entra a ruta finduser js");
  let email = req.params.rola;
  console.log("rola "+email);
  Rolas.find({"nombre_cancion":email}, function (err, users){
  //User.find({"email": mail}, function (err, users){
      console.log("Entra a find rola js");
      res.json(users);
      console.log("resultado ",users);
  });
});

userRoutes.route('/findRolaArtista/:artista').get(function (req, res) {
  let email = req.params.artista;
  console.log("rola "+email);
  Rolas.find({"artista":email}, function (err, users){
  //User.find({"email": mail}, function (err, users){
      console.log("Entra a find rola js");
      res.json(users);
      console.log("resultado ",users);
  });
});

userRoutes.route('/findArt/:rola').get(function (req, res) {
  console.log("entra a ruta find rola js");
  let rola = req.params.rola;
  //let pass = req.params.pass;
  console.log("rola "+rola);
  User.find({"artista":rola}, function (err, users){
  //User.find({"email": mail}, function (err, users){
      res.json(rola);
      console.log(rola);
  });
});

userRoutes.route('/allRolas').get(function (req, res) {
  Rolas.find(function (err, users){
    if(err){
      console.log(err);
    }
    else {
      res.json(users);
    }
  });
});

userRoutes.route('/topRolas').get(function (req, res) {
  Rolas.find(function (err, users){
    if(err){
      console.log(err);
    }
    else {
      res.json(users);
    }
  }).sort({num_visitas:-1}).limit(10);
});

userRoutes.route('/allArt').get(function (req, res) {
  Artistas.find(function (err, users){
    if(err){
      console.log(err);
    }
    else {
      res.json(users);
    }
  });
});

userRoutes.route('/topRolas').get(function (req, res) {
  Rolas.find().sort({num_visitas:1}).toArray(function (err, users){
    if(err){
      console.log(err);
    }
    else {
      
      res.json(users);
    }
  });
});

userRoutes.route('/playlist/:id').get(function (req, res) {
  let id = req.params.id;
  Playlist.find({"id_user":id},function (err, users){
    if(err){
      console.log(err);
    }
    else {
      
      res.json(users);
    }
  });
});

userRoutes.route('/Artista/:id').get(function (req, res) {
  let id = req.params.id;
  Artistas.find({"id_artista":id},function (err, users){
    if(err){
      console.log(err);
    }
    else {
      
      res.json(users);
    }
  });
});

module.exports = userRoutes;
