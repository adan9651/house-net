'use strict'

const express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    mongoose = require('mongoose'),
    config = require('./DB'),
    morgan = require('morgan');



//Middleware
const app = express();
app.use(morgan('dev'));
app.use(express.json());
app.use(cors());
//Routes
//app.use('/base',require('./route'));
app.set('port',4000);

app.listen(app.get('port'), () => { console.log("escuchando puerto ",app.get('port')) });

module.exports = app;