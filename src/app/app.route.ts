import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AltaComponent } from './alta/alta.component';
import { PerfilComponent } from './perfil/perfil.component';
import { CustomerRegComponent } from './customer-reg/customer-reg.component';
import { OwnerRegComponent } from './owner-reg/owner-reg.component';
import { ContractComponent } from './contract/contract.component';
import { HouseFormComponent } from './house-form/house-form.component';
import { AccountRecoveryComponent } from './account-recovery/account-recovery.component';

const app_routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'customer', component: CustomerRegComponent },
    { path: 'login', component: LoginComponent },
    { path: 'alta', component: AltaComponent },
    { path: 'perfil', component: PerfilComponent },
    { path: 'owner', component: OwnerRegComponent },
    { path: 'account-recovery', component: AccountRecoveryComponent },
    { path: 'contract', component: ContractComponent },
    { path: 'house-form', component: HouseFormComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot( app_routes );