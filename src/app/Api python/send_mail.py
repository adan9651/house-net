import smtplib, ssl
import email.message
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime
import time

def send_mail(data):
    user = data['user']
    nombres = data['nombres']
    apePat = data['apePat']
    apeMat = data['apeMat']
    correo = data['email']
    context = ssl.create_default_context()
    today = datetime.today ()
    tday = today.strftime ("%d-%m-%Y")
    tipo = 'Registro exitoso {user}'.format(user = user)
    username = '12.prueb.12@gmail.com'
    password = 'cuenta2019'
    html = """
    <html>
    
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
    <title>{tipo}</title>
    <style type="text/css">
    .html {
    min-height: 100%;
    position: relative;
    }

    .body {
        background-image: url(http://house-net.lightbox.mx/imagenes/textu-blanca-grande.jpg);
        margin: 0;
    }

    .h1,
    .h2,
    .h3,
    .h4,
    .h5,
    .p {
        font: Roboto;
        font-style: normal;
    }

    .card {
        background: transparent;
    }

    .card-body {
        padding: 0px;
        padding-left: 1.25rem;
        padding-right: 1.25rem;
    }

    .card-img-top {
        padding: 24px;
        background: transparent;
    }

    .float-left>h1,
    h2,
    h3,
    h4,
    h5 {
        padding-left: 20px;
    }

    .form-group>.form-control {
        margin-left: 20px;
        margin-right: 20px;
        width: 80%;
    }

    .button-recorrido {
        margin-left: 50%;
        transform: translateX(-50%);
        color: red;
    }

    .form-row {
        padding-left: 40px;
    }

    .form-check {
        margin-left: 20px;
    }

    .form-row>.col-md-auto {
        margin-right: 20px;
    }

    .col-md-4.mb-3>.form-control {
        margin-left: 50%;
        transform: translateX(-50%);
    }

    .col-6.col-md-4>.form-control {
        margin-left: 50%;
        transform: translateX(-50%);
    }

    .col-6 {
        margin-left: 20px;
    }

    .footer {
        position: relative;
        background-color: red;
        bottom: 0;
        width: 100%;
    }

    .form-ingresa {
        border: 2px solid;
        border-color: red;
        text-align: center;
        margin: 0 auto;
        padding-bottom: 20px;
        padding-top: 20px;
    }

    @media screen and (min-device-width: 1025px) {
        .float-left {
            width: 20%;
        }
        .car {
            transform: translateY(10%);
        }
        .form-ingresa {
            width: 50%;
        }
    }

    @media screen and (min-device-width: 768px) and (max-device-width: 1024px) {
        .float-left {
            width: 26%;
        }
        .car {
            transform: translateY(30%);
        }
        .form-ingresa {
            width: 70%;
        }
    }

    @media screen and (min-device-width: 320px) and (max-device-width: 767px) {
        .float-left {
            width: 100%;
        }
        .car {
            transform: translateY(10%);
        }
    }
        
    </style>
    </head>
    
    <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    </nav>
    <!--------------------Termina Navbar--------------------->
    <h2 style="padding-top: 20px; padding-bottom: 20px; color: red; text-align: center;">BIENVENIDO A HOUSENET</h2>
    <div class="car">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label> Hola """ + str(nombres) + """ """ + str(apePat) + """ """ + str(apeMat) + """ Bienvenid@ a HouseNet.</label><br>
                    <label>Es un gusto que formes parte de nuestra comunidad, esperemos que disfrutes de la mejor experiencia en el mejor lugar!</label><br>
                    <label>Email registrado: """ + str(correo) + """</label><br>
                </div>
            </div>
        </div>
    </div>
    <h2 style="padding-top: 20px; padding-bottom: 20px; color: red; text-align: center;">""" +str(tday)+ """</h2>
    <footer class="footer" style="position: fixed;">
        <div class="footer-copyright text-center py-3">
            <b><font color="white ">Todos los derechos reservados<br>© 2019 Copyright</font></b>
            <a href="http://house-net.lightbox.mx/ "><b><font color="black ">HouseNet</font></b></a>
        </div>
    </footer>
           
    </body>
    </html>
    
    
    """
    #msg = email.message.Message()
    msg = MIMEMultipart()
    msg['Subject'] = tipo
    msg['From'] = 'HouseNet<12.prueb.12@gmail.com>'
    msg['To'] = correo
    #msg.add_header('Content-Type', 'text/html')
    #msg.set_payload(email_content)
    #text = "Hola {nombres} {apePat} {apeMat}".format(nombres = nombres,apePat = apePat, apeMat = apeMat)
    #part1 = MIMEMultipart(text, 'plain')
    #index = open("index.html").read().format(nombres = nombres,apePat = apePat, apeMat = apeMat)
    #msg.attach(index)
    texto = "Hola es mi primer correo {nombres}".format(nombres = nombres)
    msg.attach(MIMEText(html, 'html'))
    try:
        server = smtplib.SMTP('smtp.gmail.com:587')
        #server.ehlo()
        server.starttls()
        server.login(username,password)
        server.sendmail(username, correo, msg.as_string())
        server.quit()
        print("mensaje enviado")
        return "200"
    except:
        print("status: error")
        return "400"

def recuperaCuenta(correo,passwd,nombres,apePat,apeMat):
    toAddr = correo
    #msg = 'cargo a etc etc'
    context = ssl.create_default_context()
    today = datetime.today ()
    tday = today.strftime ("%d-%m-%Y")
    tipo = 'Recuperacion de cuenta'
    username = '12.prueb.12@gmail.com'
    password = 'cuenta2019'
    html = """
    <html>
    
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
    <title>{tipo}</title>
    <style type="text/css">
    .html {
    min-height: 100%;
    position: relative;
    }

    .body {
        background-image: url(http://house-net.lightbox.mx/imagenes/textu-blanca-grande.jpg);
        margin: 0;
    }

    .h1,
    .h2,
    .h3,
    .h4,
    .h5,
    .p {
        font: Roboto;
        font-style: normal;
    }

    .card {
        background: transparent;
    }

    .card-body {
        padding: 0px;
        padding-left: 1.25rem;
        padding-right: 1.25rem;
    }

    .card-img-top {
        padding: 24px;
        background: transparent;
    }

    .float-left>h1,
    h2,
    h3,
    h4,
    h5 {
        padding-left: 20px;
    }

    .form-group>.form-control {
        margin-left: 20px;
        margin-right: 20px;
        width: 80%;
    }

    .button-recorrido {
        margin-left: 50%;
        transform: translateX(-50%);
        color: red;
    }

    .form-row {
        padding-left: 40px;
    }

    .form-check {
        margin-left: 20px;
    }

    .form-row>.col-md-auto {
        margin-right: 20px;
    }

    .col-md-4.mb-3>.form-control {
        margin-left: 50%;
        transform: translateX(-50%);
    }

    .col-6.col-md-4>.form-control {
        margin-left: 50%;
        transform: translateX(-50%);
    }

    .col-6 {
        margin-left: 20px;
    }

    .footer {
        position: relative;
        background-color: red;
        bottom: 0;
        width: 100%;
    }

    .form-ingresa {
        border: 2px solid;
        border-color: red;
        text-align: center;
        margin: 0 auto;
        padding-bottom: 20px;
        padding-top: 20px;
    }

    @media screen and (min-device-width: 1025px) {
        .float-left {
            width: 20%;
        }
        .car {
            transform: translateY(10%);
        }
        .form-ingresa {
            width: 50%;
        }
    }

    @media screen and (min-device-width: 768px) and (max-device-width: 1024px) {
        .float-left {
            width: 26%;
        }
        .car {
            transform: translateY(30%);
        }
        .form-ingresa {
            width: 70%;
        }
    }

    @media screen and (min-device-width: 320px) and (max-device-width: 767px) {
        .float-left {
            width: 100%;
        }
        .car {
            transform: translateY(10%);
        }
    }
        
    </style>
    </head>
    
    <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    </nav>
    <!--------------------Termina Navbar--------------------->
    <h2 style="padding-top: 20px; padding-bottom: 20px; color: red; text-align: center;">RECUPERACIÓN DE CUENTA HOUSENET</h2>
    <div class="car">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label> Hola """ + str(nombres) + """ """ + str(apePat) + """ """ + str(apeMat) + """ un gusto saludarte.</label><br>
                    <label>Te dejamos los datos de tu cuenta para poder ingresar</label><br>
                    <label>email: """ + str(correo) + """</label><br>
                    <label>password: """+ str(passwd) +"""</label><br>
                </div>
            </div>
        </div>
    </div>
    <h2 style="padding-top: 20px; padding-bottom: 20px; color: red; text-align: center;">""" +str(tday)+ """</h2>
    <footer class="footer" style="position: fixed;">
        <div class="footer-copyright text-center py-3">
            <b><font color="white ">Todos los derechos reservados<br>© 2019 Copyright</font></b>
            <a href="http://house-net.lightbox.mx/ "><b><font color="black ">HouseNet</font></b></a>
        </div>
    </footer>
           
    </body>
    </html>
    
    
    """
    #msg = email.message.Message()
    msg = MIMEMultipart()
    msg['Subject'] = tipo
    msg['From'] = 'HouseNet<12.prueb.12@gmail.com>'
    msg['To'] = correo
    #msg.add_header('Content-Type', 'text/html')
    #msg.set_payload(email_content)
    #text = "Hola {nombres} {apePat} {apeMat}".format(nombres = nombres,apePat = apePat, apeMat = apeMat)
    #part1 = MIMEMultipart(text, 'plain')
    #index = open("index.html").read().format(nombres = nombres,apePat = apePat, apeMat = apeMat)
    #msg.attach(index)
    texto = "Hola es mi primer correo {nombres}".format(nombres = nombres)
    msg.attach(MIMEText(html, 'html'))
    try:
        server = smtplib.SMTP('smtp.gmail.com:587')
        #server.ehlo()
        server.starttls()
        server.login(username,password)
        server.sendmail(username, correo, msg.as_string())
        server.quit()
        print("mensaje enviado")
        return "200"
    except:
        print("status: error")
        return "400"