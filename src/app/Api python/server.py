import methods
import send_mail
from flask import jsonify, request,  Flask
from flask_cors import CORS

app = Flask(__name__)

ip = methods.getIp()

@app.route('/')
def hello():
        return "Hola mundo!"

@app.route('/findUser', methods=["POST"])
def findUser():        
        resp = methods.findUser(request.data)
        return resp

@app.route('/findUsuario', methods=["POST"])
def findUsuario():
        resp = methods.findUsuario(request.data)
        return resp

@app.route('/findNumTarjeta', methods=["POST"])
def findNumTarjeta():
        resp = methods.findNumTarjeta(request.data)
        return resp

@app.route('/recuperaCuenta', methods=["POST"])
def recuperaCuenta():
        resp = methods.recuperaCuenta(request.data)
        return resp

@app.route('/addCustomer',methods=["POST"])
def addUser():
        resp = methods.addCustomer(request.data)
        return resp

@app.route('/addHouse',methods=["POST"])
def addHouse():
        resp = methods.addHouse(request.data)
        return resp

@app.route('/findUsername',methods=["POST"])
def findUsername():
        resp = methods.findUsername(request.data)
        return resp

@app.route('/validateCard',methods=["POST"])
def validateCard():
        resp = methods.validateCard(request.data)
        return resp

@app.route('/realEstate',methods=["POST"])
def realEstate():
        resp = methods.realEstate(request.data)
        return resp

@app.route('/getAllRealEstates',methods=["POST"])
def getAllRealEstates():
        resp = methods.getAllRealEstates(request.data)
        return resp

@app.route('/removeUser',methods=["POST"])
def get_prod():
        resp = methods.removeUser(request.data)
        return resp

@app.route('/editUser', methods=["POST"])
def actividades():
        resp = methods.editUser(request.data)
        return resp

@app.route('/dos')
def dos():
        resp = methods.dos()
        return resp, 200
        
@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp

CORS(app)
if __name__ == "__main__":
        app.run()