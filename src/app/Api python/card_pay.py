import subprocess
import os
number = 5555555555554444
expMonth = 11
expYear = 24
cvc = 123        
amount = 100
currency = "USD"
description = "Welcome_to_HouseNet"
def validateCard(num):
    number = num
    try:
        batcmd="ruby send_pay.rb {number} {expMonth} {expYear} {cvc} {amount} {currency} {description}".format(
            number = number, expMonth = expMonth, expYear = expYear, cvc = cvc, amount = amount, currency = currency, description = description)
        result = subprocess.check_output(batcmd, shell=True)
        status = result.decode('utf-8')
        print(status)
        if( status ):
            return "200"
        else:
            return "300"
    except:
        return "400"
