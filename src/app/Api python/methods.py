from db import mongo
import json
from bson.json_util import dumps
from bson.objectid import ObjectId
from flask import request
from datetime import datetime
import socket
import re
import smtplib, ssl
import email.message
import send_mail
import card_pay

def getIp():
    ip = socket.gethostbyname(socket.gethostname())
    return ip

def dump(query):
        json = dumps(query,indent=1, ensure_ascii=False).encode('utf8')
        return json

def addCustomer(data):
    res = json.loads(data)
    mongo.customer.insert(res)
    send_mail.send_mail(res)
    return "200"

def addHouse(data):
    res = json.loads(data)
    mongo.inmobiliarios.insert(res)
    return "200"

def findUser(data):
    res = json.loads(data)
    email = str(res['email'])
    res = mongo.customer.find_one({'email':email})
    result = dump(res)
    return result

def findUsuario(data):
    res = json.loads(data)
    email = str(res['user'])
    res = mongo.customer.find_one({'user':email})
    result = dump(res)
    return result

def findNumTarjeta(data):
    res = json.loads(data)
    numTarjeta = int(res['numTarjeta'])
    res = mongo.customer.find_one({'numTarjeta':numTarjeta})
    result = dump(res)
    return result

def findUsername(data):
    res = json.loads(data)
    username = str(res['username'])
    res = mongo.customer.find_one({'username':username})
    result = dump(res)
    return result



def recuperaCuenta(data):
    res = json.loads(data)
    email = str(res['email'])
    password = str(res['password'])
    nombres = str(res['nombres'])
    apePAt = str(res['apePat'])
    apeMat = str(res['apeMat'])
    resp = send_mail.recuperaCuenta(email,password,nombres,apePAt,apeMat)
    return resp

def realEstate(data):
    res = json.loads(data)
    minMensualidad = (int(res['minMensualidad'])*1000)
    maxMensualidad = (int(res['maxMensualidad'])*1000)
    minHabitaciones = int(res['minHabitaciones'])
    maxHabitaciones = int(res['maxHabitaciones'])
    minTiempo = int(res['minTiempo'])
    maxTiempo = int(res['maxTiempo'])
    bano = res['bano']
    tipo = res['tipo']
    print(bano)
    query = {"$and":[
        {"mensualidad":{"$gte":minMensualidad}},
        {"mensualidad":{"$lte":maxMensualidad}},
        {"numHabitaciones":{"$gte":minHabitaciones}},
        {"numHabitaciones":{"$lte":maxHabitaciones}},
        {"tiempo":{"$gte":minTiempo}},
        {"tiempo":{"$lte":maxTiempo}},
        {"bano":bano},
        {"tipo":tipo}
        ]}
    resp = mongo.inmobiliarios.find(query)
    result = dump(resp)
    return result

def getAllRealEstates(data):
    res = json.loads(data)
    resp = mongo.inmobiliarios.find(res)
    result = dump(resp)
    return result

def removeUser(data):
    res = json.loads(data)
    username = res['username']
    mongo.user.remove({'username':username})
    return "200"

def validateCard(data):
    res = json.loads(data)
    number = res['number']
    resp = card_pay.validateCard(number)
    return resp

def editUser(data):
    res = json.loads(data)
    mongo.user.update_one({
        'username': res['username']},{
        '$set':{'calle': res['calle'],'numero_ext': res['numero_ext'],'fraccionamiento': res['fraccionamiento'],
                'municipio': res['municipio'],'estado': res['estado'], 'cp': res['cp'], 'celular': res['celular']}
    })
    return "200"

def code(data):
    res = json.loads(data)
    pipeline = [
	    {'$match':{'_id':ObjectId(res['id'])}},
        {
        '$lookup': {
            'from': 'clasifica',
            'localField': 'codigo_act',
            'foreignField': 'codigo',
            'as': 'clasifica'}
        },
        {
        '$unwind': '$clasifica'
        },
        {
        '$project': {
            'productos':'$clasifica.productos',
            "_id":1,"nom_estab":1,"codigo_act":1,"nom_vial":1,"tipo_vial":1,"nomb_asent":1,"cod_postal":1,"location":1,"numero_ext":1,
            "municipio":1,"entidad":1,"telefono":1}
        },{'$limit':1}
    ]
    result = mongo.actividades.aggregate(pipeline)
    resp = dump(result)
    return resp

def clasi():
    res = mongo.scian.find({}).limit(1)
    resp = dump(res)
    return resp

def acti():
    res = mongo.denue.find().limit(1)
    resp = dump(res)
    return resp

def dos():
    resp = "hola segunda prueba"
    return resp
