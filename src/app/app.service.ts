import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class Service{

    //url = 'http://172.16.105.80:5000';
    url = 'http://localhost:5000';

    constructor(private http : HttpClient){}

    addCustomer(customer){
        const obj = {
            nombres: customer.nombres,
            apePat: customer.apePat,
            apeMat: customer.apeMat,
            user: customer.user,
            password: customer.password,
            calle: customer.calle,
            numExt: customer.numExt,
            numInt: customer.numInt,
            fraccionamiento: customer.fraccionamiento,
            municipio: customer.municipio,
            estado: customer.estado,
            codPostal: customer.codPostal,
            email: customer.email,
            telefono: customer.telefono,
            numTarjeta: customer.numTarjeta,
            vencimientoMM: customer.vencimientoMM,
            vencimientoAA: customer.vencimientoAA,
            cvv: customer.cvv
        }
        return this
        .http
        .post(`${this.url}/addCustomer`,obj).toPromise();
    }

    addHouse(house) {
        const obj = {
            calle: house.calle,
            num: house.num,
            colonia: house.colonia,
            cp: house.cp,
            estado: house.estado,
            numHabitaciones: house.numHabitaciones,
            bano: house.bano,
            tiempo: house.tiempo,
            tipo: house.tipo,
            pisos: house.pisos,
            piscina: house.piscina,
            garage: house.garage,
            jardin: house.jardin,
            mensualidad: house.mensualidad,
            url: house.url,
            icon: house.icon,
            owner: house.owner
        }
        return this
        .http
        .post(`${this.url}/addHouse`, obj).toPromise();
    }

    buscaCasas(inmobiliario){
        const obj = {
            estado: inmobiliario.estado,
            maxHabitaciones: inmobiliario.maxHabs,
            minHabitaciones: inmobiliario.minHabs,
            maxMensualidad: inmobiliario.maxMens,
            minMensualidad: inmobiliario.minMens,
            maxTiempo: inmobiliario.maxTiempo,
            minTiempo: inmobiliario.minTiempo,
            bano: inmobiliario.bano,
            tipo: inmobiliario.tipo
        }
        return this
        .http
        .post(`${this.url}/realEstate`,obj).toPromise();
    }

    elimina_user(username){
        const obj = {
            username: username
        }
        return this
        .http
        .post(`${this.url}/removeUser`,obj).toPromise();
    }

    findUser(email){
        const obj = {
            email: email
        }
        return this
        .http
        .post(`${this.url}/findUser`,obj).toPromise();
    }

    findUsuario(user){
        const obj = {
            user: user
        }
        return this
        .http
        .post(`${this.url}/findUsuario`,obj).toPromise();
    }

    findNumTarjeta(numTarjeta){
        const obj = {
            numTarjeta: numTarjeta
        }
        return this
        .http
        .post(`${this.url}/findNumTarjeta`,obj).toPromise();
    }

    recuperaCuenta(email,password,nombres,apePat,apeMat){
        const obj = {
            email: email,
            password: password,
            nombres: nombres,
            apePat: apePat,
            apeMat: apeMat
        }
        return this
        .http
        .post(`${this.url}/recuperaCuenta`,obj).toPromise();
    }

    addUser(username, pass, nombre, ape_pat, ape_mat, fecha_nac, calle, numero_ext, fraccionamiento, municipio, estado, cp, email, celular ){
        const obj = {
            username: username,
            pass: pass,
            nombre: nombre,
            ape_pat: ape_pat,
            ape_mat: ape_mat,
            fecha_nac: fecha_nac,
            calle: calle,
            numero_ext: numero_ext,
            fraccionamiento: fraccionamiento,
            municipio: municipio,
            estado: estado,
            cp: cp,
            email: email,
            celular: celular
        };
        console.log(obj);
        this.http.post(`${this.url}/addUser`,obj).subscribe(res=> console.log('Done'));
    }

    validateCard(number){
        const obj={
            number: number
        }
        return this
        .http
        .post(`${this.url}/validateCard`,obj).toPromise();
    }

    getAllRealEstates(estado){
        const obj={
            estado: estado
        }
        return this
        .http
        .post(`${this.url}/getAllRealEstates`,obj).toPromise();
    }

    editUser(username,calle,numero_ext,fraccionamiento,municipio,estado,cp,celular){
        const obj = {
            username: username,
            calle: calle,
            numero_ext: numero_ext,
            fraccionamiento: fraccionamiento,
            municipio: municipio,
            estado: estado,
            cp: cp,
            celular: celular
        }
        return this
        .http
        .post(`${this.url}/editUser`,obj).toPromise();
    }
      
    editBusiness(id) {
        return this
            .http
            .get(`${this.url}/edit/${id}`);
    }

    updateBusiness(person_name, business_name, business_gst_number, id) {
        console.log("entra a update");
        const obj = {
            person_name: person_name,
            business_name: business_name,
            business_gst_number: business_gst_number
          };
        this
          .http
          .post(`${this.url}/update/${id}`, obj)
          .subscribe(res => console.log('Done'));
    }

    deleteBusiness(id){
        return this.http.get(`${this.url}/delete/${id}`);
    }

    findMaxUser(){
        return this
            .http
            .get(`${this.url}/findMaxUser`).toPromise();
    }

    allRolas(){
        return this.http.get(`${this.url}/allRolas`).toPromise();
    }

    findRola(rola){
        return this
        .http
        .get(`${this.url}/findRola/${rola}`).toPromise();
    }

    playlist(artista){
        return this
        .http
        .get(`${this.url}/playlist/${artista}`).toPromise();
    }

    findArtista(artista){
        return this
        .http
        .get(`${this.url}/Artista/${artista}`).toPromise();
    }
    
    findRolas_artista(artista){
        return this
        .http
        .get(`${this.url}/findRolaArtista/${artista}`).toPromise();
    }

    top_rolas(){
        return this
        .http
        .get(`${this.url}/topRolas/`).toPromise();
    }

    suscripcion(id){
        return this
        .http
        .get(`${this.url}/suscripcion/${id}`).toPromise();
    }

}