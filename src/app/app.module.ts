import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { APP_ROUTING } from './app.route';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { Service } from './app.service';
import { HomeComponent } from './home/home.component';
import { AltaComponent } from './alta/alta.component';
import { PerfilComponent } from './perfil/perfil.component';
import { CustomerRegComponent } from './customer-reg/customer-reg.component';
import { OwnerRegComponent } from './owner-reg/owner-reg.component';
import { AccountRecoveryComponent } from './account-recovery/account-recovery.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxPaginationModule } from 'ngx-pagination';
import { ContractComponent } from './contract/contract.component';
import { HouseFormComponent } from './house-form/house-form.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AltaComponent,
    PerfilComponent,
    CustomerRegComponent,
    OwnerRegComponent,
    AccountRecoveryComponent,
    ContractComponent,
    HouseFormComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    APP_ROUTING,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule,
    NgxSpinnerModule
  ],
  providers: [Service],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
  constructor( ) { }
 }
