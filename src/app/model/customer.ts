export class Customer{
    nombres: string;
    apePat: string;
    apeMat: string;
    user: string;
    password: string;
    calle: string;
    numExt: number;
    numInt: string;
    fraccionamiento: string;
    municipio: string;
    estado: string;
    codPostal: number;
    email: string;
    telefono: number;
    numTarjeta: number;
    vencimiento: string;
    cvv: number;
}