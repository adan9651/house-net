export class HouseFormComponent{
    calle: string;
    num: number;
    colonia: string;
    cp: number;
    estado: string;
    numHabitaciones: number;
    bano: number;
    tiempo: number;
    tipo: string;
    pisos: number;
    piscina: string;
    garage: string;
    jardin: string;
    mesualidad: number;
    url: string;
    icon: string;
    owner: string;
}