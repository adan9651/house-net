import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Service } from '../app.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  constructor( private service: Service, private router: Router, private http: HttpClient ) { }
  cache;
  public user
  async ngOnInit() {
    var email = localStorage.getItem('currentUser');
    //const res = await this.service.findUser(email);
    //this.user = res;
  }

  estados = [
    {cod:"01",nombre:"Aguascalientes"},
    {cod:"02",nombre:"Baja California"},
    {cod:"03",nombre:"Baja California Sur"},
    {cod:"04",nombre:"Campeche"},
    {cod:"05",nombre:"Coahuila"},
    {cod:"06",nombre:"Colima"},
    {cod:"07",nombre:"Chiapas"},
    {cod:"08",nombre:"Chihuahua"},
    {cod:"09",nombre:"Distrito Federal"},
    {cod:"10",nombre:"Durango"},
    {cod:"11",nombre:"Guanajuato"},
    {cod:"12",nombre:"Guerrero"},
    {cod:"13",nombre:"Hidalgo"},
    {cod:"14",nombre:"Jalisco"},
    {cod:"15",nombre:"Mexico"},
    {cod:"16",nombre:"Michoacan de Ocampo"},
    {cod:"17",nombre:"Morelos"},
    {cod:"18",nombre:"Nayarit"},
    {cod:"19",nombre:"Nuevo Leon"},
    {cod:"20",nombre:"Oaxaca"},
    {cod:"21",nombre:"Puebla"},
    {cod:"22",nombre:"Queretaro"},
    {cod:"23",nombre:"Quintana Roo"},
    {cod:"24",nombre:"San Luis Potosi"},
    {cod:"25",nombre:"Sonora"},
    {cod:"26",nombre:"Tabasco"},
    {cod:"27",nombre:"Tamaulipas"},
    {cod:"28",nombre:"Tlaxcala"},
    {cod:"29",nombre:"Veracruz"},
    {cod:"30",nombre:"Yucatan"},
    {cod:"31",nombre:"Zacatecas"}]

  eliminar(){
    localStorage.clear();
    this.service.elimina_user(this.user['username']);
    alert("El perfil de usuario "+ this.user['username'] + " se ha eliminado correctamente");
    this.router.navigate(['/home']);
  }

  editar(calle, numero_ext, fraccionamiento, municipio, estado, cp, celular){
    this.service.editUser(this.user['username'],calle,numero_ext,fraccionamiento,municipio,estado,cp,celular);
    alert("perfil actualizado correctamente");
    window.location.reload();
  }

  goHome(){
    this.router.navigate(['/home']);
  }

}
